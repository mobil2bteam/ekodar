//
//  NewsViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit


class NewsViewController: RootViewController, UITableViewDelegate {
    @IBOutlet weak var newsTableView: UITableView!
    var newsDataSource: DataSource!
    let cellIdentifier = "NewsTableViewCell"

    // MARK: - Lifecycle 
    convenience init(showHamburgerButton: Bool) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.showHamburgerButton = showHamburgerButton
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Новости"
        prepareNewsTableView()
    }
    
    // MARK: - Methods
    func prepareNewsTableView() {
        let block: DataSourceConfigureBlock = { (cell, news) in
            if let newsCell = cell as? NewsTableViewCell,
                let newsItem = news as? NewsModel {
                newsCell.configure(for: newsItem)
            }
        }
        newsTableView.register(NewsTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        newsDataSource = DataSource(items: AppManager.shared.options.news, cellIdentifier: cellIdentifier, configureBlock: block)
        newsTableView.dataSource = newsDataSource
        newsTableView.tableFooterView = UIView.init()
    }
    
    func showNewsViewController(news: NewsModel) {
        let newsDetailViewController = NewsDetailViewController.init(news: news)
        navigationController?.pushViewController(newsDetailViewController, animated: true)
    }

    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let news = newsDataSource.item(at: indexPath as NSIndexPath) as! NewsModel
        showNewsViewController(news: news)
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

