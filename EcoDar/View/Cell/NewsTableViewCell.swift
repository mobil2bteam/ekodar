//
//  NewsTableViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = ""
        descriptionLabel.text = ""
        newsImageView.image = nil
    }
    
    func configure(for news: NewsModel) {
        nameLabel.text = news.name
        descriptionLabel.text = news.add_text
        let imageUrl = URL(string: news.image_big ?? "")!
        newsImageView.af_setImage(withURL: imageUrl)
    }
}
