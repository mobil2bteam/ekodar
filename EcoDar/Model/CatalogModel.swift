//
//  CatalogModel.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import ObjectMapper

class CatalogModel: Mappable {
    var id: Int!
    var parent_id: Int?
    var text: String?
    var image: String?
    var name: String?
    var icon: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        parent_id    <- map["parent_id"]
        text    <- map["text"]
        image    <- map["image"]
        name    <- map["name"]
        icon    <- map["icon"]

    }
}
