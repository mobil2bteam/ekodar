//
//  CatalogTableViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class CatalogTableViewCell: UITableViewCell {

    @IBOutlet weak var catalogImageView: UIImageView!
    @IBOutlet weak var catalogLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureFor(catalog: CatalogModel) {
        catalogLabel.text = catalog.name
        if let imageUrl = URL(string: catalog.icon ?? "") {
            catalogImageView.af_setImage(withURL: imageUrl)
        }
    }
}
