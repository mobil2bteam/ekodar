//
//  NotificationManager.swift
//  EcoDar
//
//  Created by Ruslan on 10/6/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class NotificationManager {
    static let days = 7
    
    /**
     Register notifications. For iOS >= 10.0 - UserNotifications, else UILocalNotification
     */
    class func registerLocalNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { (granted, error) in
                if error == nil {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings.init(types: [.alert, .badge, .sound], categories: nil))
        }
    }
    
    /**
     Remove all notifications
     */
    class func removeAllNotifications() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests()
            center.removeAllDeliveredNotifications()
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    /**
     Generate new notifications for schedule
     - parameter route: Route, which contains shcedule for notifications
     */
    class func generateNotifications(for route:RouteModel) {
        route.schedule[1].time_from = "10:34"
        route.schedule[1].time_to = "20:34"

        if route.schedule.count > 0 {
            var date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.init(identifier: "ru_RU")
            dateFormatter.dateFormat  = "EEEE"
            for _ in 0..<days {
                let dayInWeek = dateFormatter.string(from: date as Date)
                // if user disabled notification for today
                if  NSCalendar.current.isDateInToday(date) && route.hasOrderedToday() {
                    date = date.addingTimeInterval(24 * 60 * 60)
                    continue
                }
                for day in route.schedule {
                    // if schedule contains this day
                    if dayInWeek.caseInsensitiveCompare(day.week_name) == .orderedSame {
                        generateNotifications(date: date, schedule: day)
                    }
                }
                date = date.addingTimeInterval(24 * 60 * 60)
            }
        }
    }

    /**
     Generate notifications for date with schedule. Notifications will be showing every hour from start time to finish time
     - parameter date: Date for notifications
     - parameter schedule: Schedule
     */
    class func generateNotifications(date:Date, schedule: ScheduleModel) {
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat  = "d.MM.y HH:mm"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d.MM.y"
        
        let formattedDate = dateFormatter.string(from: date)
        
        guard var startTime = dateTimeFormatter.date(from: "\(formattedDate) \(schedule.time_from!)"),
        let finishTime = dateTimeFormatter.date(from: "\(formattedDate) \(schedule.time_to!)") else {
            return
        }
        // notificate user at 1 hour before start time and each hour until finish time
        startTime = startTime.addingTimeInterval(-60 * 60)
        while startTime.compare(finishTime) == .orderedAscending {
            if startTime.compare(Date()) == .orderedDescending {
                addNotification(fireDate: startTime, time_to: schedule.time_to!)
            }
            startTime = startTime.addingTimeInterval(60 * 60)
        }
    }

    
    /**
     Add notification
     - parameter fireDate: Date to fire notification
     - parameter time_to: Time to which user should make order
     */
    class func addNotification(fireDate: Date, time_to: String, alert: String = "Внимание!") {
        let message = "Не забудьте сегодня сделать заказ до \(time_to)"
        if #available(iOS 10.0, *) {
            let localNotification = UNMutableNotificationContent()
            localNotification.title = alert
            localNotification.body = message
            localNotification.sound = UNNotificationSound(named: "cow.wav")

            let dateTimeFormatter = DateFormatter()
            dateTimeFormatter.dateFormat  = "d.MM.y HH:mm"
            let identifier = dateTimeFormatter.string(from: fireDate)
            let dateCompenents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: fireDate)
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateCompenents, repeats: false)
            localNotification.badge = (UIApplication.shared.applicationIconBadgeNumber + 1) as NSNumber
            
            let request = UNNotificationRequest(identifier: identifier, content: localNotification, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                if error != nil {

                } else {
                    print("success")
                }
            })
        } else {
            let localNotification = UILocalNotification()
            localNotification.fireDate = fireDate
            localNotification.timeZone = NSTimeZone.system
            localNotification.repeatInterval = NSCalendar.Unit(rawValue: 0)
            localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
            localNotification.alertTitle = alert
            localNotification.alertBody = message
            localNotification.soundName = "cow.wav"
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
    }
}
