//
//  DataManager.swift
//  EcoDar
//
//  Created by Ruslan on 9/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import Foundation
import RealmSwift

let uiRealm = try! Realm()

class DataManager: NSObject {
    
    static let notificationName = Notification.Name("DataManagerNotificationIdentifier")

    class func addProduct(product: ProductModel, count: Int = 1) {
        if let existProduct = uiRealm.object(ofType: ProductRealmModel.self, forPrimaryKey: product.id) {
            try! uiRealm.write{
                if count == 1 {
                    existProduct.count += 1
                } else {
                    existProduct.count = count
                }
                if count == 0 {
                    uiRealm.delete(existProduct)
                }
            }
        } else {
            if count == 0 {
                return
            }
            let newProduct = ProductRealmModel()
            newProduct.id = product.id
            newProduct.name = product.name
            newProduct.count = count
            if product.images.count > 0 {
                newProduct.image = product.images[0]
            } else {
                newProduct.image = product.image
            }
            if product.prices.count > 0 {
                newProduct.price = product.prices[0].price
                newProduct.offer_id = product.prices[0].offer_id ?? 0
            }
            try! uiRealm.write{
                uiRealm.add(newProduct)
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func decreaseProduct(productId: Int) {
        if let existProduct = uiRealm.object(ofType: ProductRealmModel.self, forPrimaryKey: productId) {
            try! uiRealm.write{
                if existProduct.count > 1 {
                    existProduct.count -= 1
                } else {
                    uiRealm.delete(existProduct)
                }
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func updateProduct(productId: Int, count: Int) {
        if let existProduct = uiRealm.object(ofType: ProductRealmModel.self, forPrimaryKey: productId) {
            try! uiRealm.write{
                existProduct.count = count
                if existProduct.count == 0 {
                    uiRealm.delete(existProduct)
                }
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func deleteProduct(product: ProductRealmModel) {
        try! uiRealm.write{
            uiRealm.delete(product)
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func countFor(product: ProductModel) -> Int {
        if let existProduct = uiRealm.object(ofType: ProductRealmModel.self, forPrimaryKey: product.id) {
            return existProduct.count
        }
        return 0
    }
    
    class func products() -> Results<ProductRealmModel> {
        return uiRealm.objects(ProductRealmModel.self)
    }
    
    class func totalCount() -> Int {
        let products = uiRealm.objects(ProductRealmModel.self)
        var totalCount = 0
        for product in products {
            totalCount += product.count
        }
        return totalCount
    }

    class func clear() {
        try! uiRealm.write{
            uiRealm.delete(products())
        }
    }
    
    class func totalPrice() -> Int {
        var totalPrice = 0
        for product in products() {
            totalPrice += product.count * product.price
        }
        return totalPrice
    }
}
