//
//  ProductsViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SVProgressHUD


class ProductsViewController: RootViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    var products: [ProductModel] = []
    let cellIdentifier = "ProductCollectionViewCell"
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: Lifecycle
    convenience init(products: [ProductModel]) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.products = products
        self.showCartButton = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Товары"
        prepareCollectionView()
        self.collectionView!.register(ProductCollectionViewCell.nib(), forCellWithReuseIdentifier: cellIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(ProductsViewController.updateProductCount), name: DataManager.notificationName, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: DataManager.notificationName, object: nil)
    }

    // MARK: Methods
    func updateProductCount() {
        if showCartButton {
            let number = DataManager.totalCount()
            navigationItem.rightBarButtonItem?.setBadge(text: "\(number)")
        }
    }

    func loadProduct(productId: Int) {
        SVProgressHUD.show()
        weak var weakSelf = self
        APIManager.getProductFor(productId: productId, successHandler: { (product) in
            SVProgressHUD.dismiss()
            weakSelf?.showProductController(for: product)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    func reloadProducts() {
        let number = DataManager.totalCount()
        navigationItem.rightBarButtonItem?.setBadge(text: "\(number)")
    }

    func prepareCollectionView() {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        let width = (UIScreen.main.bounds.width - 30) / 2
        let height = width * 1.5
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 10
        collectionView.collectionViewLayout = layout
    }
    
    func showProductController(for product: ProductModel) {
        let vc = ProductViewController.init(product: product)
        navigationController?.pushViewController(vc, animated: true)
    }

}

// MARK: UICollectionViewDataSource
extension ProductsViewController {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ProductCollectionViewCell
        cell.configure(for: products[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        loadProduct(productId: product.id)
    }
}

