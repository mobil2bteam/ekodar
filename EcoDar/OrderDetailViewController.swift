//
//  OrderDetailViewController.swift
//  EcoDar
//
//  Created by Ruslan on 10/5/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var orderButton: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    var order: OrderModel!
    let cellIdentifier = "CartTableViewCell"

    // MARK: - Lifecyle
    convenience init(order: OrderModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.order = order
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Заказ"
        dateLabel.textColor = UIColor("#C7C7C7")
        dateLabel.text = order.createdAt
        orderNumberLabel.text = "Заказ №\(order.id)"
        totalPriceLabel.text = "\(order.total!) руб."
        countLabel.text = "\(order.products.count) шт."
        orderButton.backgroundColor = UIColor.accentColor()
        cartTableView.register(CartTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
    }
    
    // MARK: - Actions
    @IBAction func orderButtonPressed(_ sender: Any) {
        weak var weakSelf = self
        let cartProducts = DataManager.products()
        if cartProducts.count > 0 {
            let orderAlert = RepeatOrderAlertViewController.init()
            orderAlert.modalPresentationStyle = .overFullScreen;
            orderAlert.modalTransitionStyle = .crossDissolve;
            orderAlert.completionHandler = { addCartProducts in
                if !addCartProducts {
                    DataManager.clear()
                }
                weakSelf?.showCart()
            }
            present(orderAlert, animated: true, completion: nil)
        } else {
            showCart()
        }
    }
    
    // MARK: - Methods
    func showCart() {
        for product in order.products {
            if product.prices.count > 0 {
                DataManager.addProduct(product: product, count: product.prices[0].count!)
            }
        }
        navigationController?.pushViewController(CartViewController.init(), animated: true)
    }
}


extension OrderDetailViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CartTableViewCell
        let product = order.products[indexPath.row]
        cell.configure(orderProduct: product)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

