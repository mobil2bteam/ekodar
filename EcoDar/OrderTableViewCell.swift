//
//  OrderTableViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var orderLabel: UILabel!
    var products: [ProductModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        detailButton.setTitleColor(UIColor.primaryColor(), for: .normal)
        dateLabel.textColor = UIColor("#C7C7C7")
        imagesCollectionView.register(ImageCollectionViewCell.nib(), forCellWithReuseIdentifier: "cell")
        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(for order: OrderModel) {
        orderLabel.text = "Заказ №\(order.id)"
        dateLabel.text = order.createdAt
        priceLabel.text = "\(order.total!) руб."
        countLabel.text = "\(order.products.count) шт."
        products = order.products
        imagesCollectionView.reloadData()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        products = []
    }
}

// MARK: UICollectionViewDataSource
extension OrderTableViewCell {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        cell.configure(for: products[indexPath.row].image)
        cell.tag = indexPath.item
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
}

