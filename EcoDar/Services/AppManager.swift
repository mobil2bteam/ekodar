//
//  AppManager.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class AppManager: NSObject {
    
    static let shared: AppManager = {
        return AppManager()
    }()
        
    var options: OptionsModel!
}
