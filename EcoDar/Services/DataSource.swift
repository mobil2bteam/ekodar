//
//  DataSource.swift
//  EcoDar
//
//  Created by Ruslan on 10/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import Foundation

typealias DataSourceConfigureBlock = (AnyObject, AnyObject) -> ()

class DataSource: NSObject, UITableViewDataSource, UICollectionViewDataSource {
    
    private var items: [Any]
    private var cellIdentifier: String
    private var configureBlock: DataSourceConfigureBlock

    init(items: [Any], cellIdentifier: String, configureBlock: @escaping DataSourceConfigureBlock) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.configureBlock = configureBlock
    }
    
    func item(at indexPath: NSIndexPath) -> Any {
        return items[indexPath.row]
    }

    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let item = self.item(at: indexPath as NSIndexPath)
        configureBlock(cell, item as AnyObject)
        return cell
    }

    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        let item = self.item(at: indexPath as NSIndexPath)
        configureBlock(cell, item as AnyObject)
        return cell
    }

}
