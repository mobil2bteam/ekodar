//
//  OrderModel.swift
//  EcoDar
//
//  Created by Ruslan on 10/4/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderListModel: Mappable {
    var archive: [OrderModel] = []
    var actual: [OrderModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        archive    <- map["archive"]
        actual    <- map["actual"]
    }
    
}

class OrderModel: NSObject, NSCopying, Mappable {
    var id: Int = 0
    var total: Int!
    var createdAt: String!
    var products: [ProductModel] = []
    var isPayment: Bool!
    
    required init?(map: Map) {
        
    }

    init(id: Int, total: Int, createdAt: String, products: [ProductModel], isPayment: Bool) {
        super.init()
        self.id = id
        self.total = total
        self.createdAt = createdAt
        self.products = products
        self.isPayment = isPayment
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        total    <- map["total"]
        createdAt    <- map["createdAt"]
        isPayment    <- map["isPayment"]
        products <- map["products"]
    }
    
    required init(_ model: OrderModel) {
        id = model.id
        createdAt = model.createdAt
        total = model.total
        let originalArray = model.products as NSArray
        let duplicateArray = NSArray(array:originalArray as! [ProductModel], copyItems: true)
        products = duplicateArray as! [ProductModel]
        isPayment = model.isPayment
    }

    
    func copy(with zone: NSZone? = nil) -> Any {
        return type(of:self).init(self)
    }

}
