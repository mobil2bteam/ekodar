//
//  MainViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD
import AlamofireImage
import MessageUI


class MainViewController: RootViewController, UITableViewDelegate, MFMailComposeViewControllerDelegate {
    let cellIdentifier = "CatalogTableViewCell"
    let hideAnimationDuration = 0.25
    var catalogDataSource: DataSource!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var newsButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var writeButton: UIButton!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var catalogTableView: UITableView!
    @IBOutlet weak var notificationLabel: UILabel!

    // MARK: - Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.showHamburgerButton = true
        self.showCartButton = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ЭкоДар"
        prepareViews()
        prepareCatalogTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Check notification for user
        let notification = AppManager.shared.options.user!.hasNotificationForToday()
        notificationView.isHidden = !notification.shouldShow
        notificationLabel.text = notification.message
    }
    
    // MARK: - Methods
    func prepareCatalogTableView() {
        let block: DataSourceConfigureBlock = { (cell, item) in
            if let catalogCell = cell as? CatalogTableViewCell,
                let catalog = item as? CatalogModel {
                catalogCell.configureFor(catalog: catalog)
            }
        }
        catalogTableView.register(CatalogTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        catalogDataSource = DataSource(items: AppManager.shared.options.catalog, cellIdentifier: cellIdentifier, configureBlock: block)
        catalogTableView.dataSource = catalogDataSource
        catalogTableView.tableFooterView = UIView.init()
    }
    
    func prepareViews() {
        // Buttons
        historyButton.setBackgroundImage(UIImage.init(color: UIColor.historyButtonBackgroundColor()), for: .normal)
        historyButton.setBackgroundImage(UIImage.init(color: UIColor.historyButtonBackgroundColorHighlighted()), for: .highlighted)
        callButton.setBackgroundImage(UIImage.init(color: UIColor.callButtonBackgroundColor()), for: .normal)
        callButton.setBackgroundImage(UIImage.init(color: UIColor.callButtonBackgroundColorHighlighted()), for: .highlighted)
        writeButton.setBackgroundImage(UIImage.init(color: UIColor.writeButtonBackgroundColor()), for: .normal)
        writeButton.setBackgroundImage(UIImage.init(color: UIColor.writeButtonBackgroundColorHighlighted()), for: .highlighted)
        newsButton.setBackgroundImage(UIImage.init(color: UIColor.newsButtonBackgroundColor()), for: .normal)
        newsButton.setBackgroundImage(UIImage.init(color: UIColor.newsButtonBackgroundColorHighlighted()), for: .highlighted)
        // Views
        notificationView.backgroundColor = UIColor.accentColor()
        notificationButton.titleLabel?.textAlignment = .center
    }
    
    // MARK: - Actions
    @IBAction func historyButtonPressed(_ sender: Any) {
        loadOrders()
    }
    
    @IBAction func newsButtonPressed(_ sender: Any) {
        let newsViewController = NewsViewController.init(showHamburgerButton: false)
        navigationController?.pushViewController(newsViewController, animated: true)
    }
    
    @IBAction func callButtonPressed(_ sender: Any) {
        let phoneAlert = PhoneAlertViewController.init()
        phoneAlert.modalPresentationStyle = .overFullScreen
        phoneAlert.modalTransitionStyle = .crossDissolve
        present(phoneAlert, animated: true, completion: nil)
    }
    
    @IBAction func writeButtonPressed(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            alert(message: "Ваше устройство не поддерживает отправку писем")
            return
        }
        sendEmail()
    }

    @IBAction func notificationButtonPressed(_ sender: Any) {
        AppManager.shared.options.user!.removeNotificationForToday()
        hideNotificationView()
    }
    
    @IBAction func notificationViewPressed(_ sender: Any) {
        hideNotificationView()
    }
    
    // MARK: - Methods
    func hideNotificationView() {
        showNotificationView = false
        weak var weakSelf = self
        UIView.animate(withDuration: hideAnimationDuration) {
            weakSelf?.notificationView.isHidden = true
        }
    }
    
    func loadProducts(catalogId: Int) {
        SVProgressHUD.show()
        weak var weakSelf = self
        APIManager.getProductsFor(catalogId: catalogId, successHandler: { (products) in
            SVProgressHUD.dismiss()
            let vc = ProductsViewController.init(products: products)
            weakSelf?.navigationController?.pushViewController(vc, animated: true)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error.localizedDescription)
        }
    }
    
    func loadOrders() {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.orders(successHandler: { (orderList) in
            let vc = HistoryViewController.init(orders: orderList.actual + orderList.archive, showHamburgerButton: false)
            weakSelf?.navigationController?.pushViewController(vc, animated: true)
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let catalog = catalogDataSource.item(at: indexPath as NSIndexPath) as! CatalogModel
        tableView.deselectRow(at: indexPath, animated: true)
        loadProducts(catalogId: catalog.id)
    }
}

// MARK: MFMailComposeViewControllerDelegate
extension MainViewController {
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["info@ekodar.ru"])
        composeVC.setSubject("Сообщение с мобильного приложения")
        composeVC.setMessageBody("", isHTML: false)
        present(composeVC, animated: true, completion: nil)
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
