//
//  PhoneAlertViewController.swift
//  EcoDar
//
//  Created by Ruslan on 10/6/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class PhoneAlertViewController: UIViewController {

    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    // MARK: Actions
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func operatorCallButtonPressed(_ sender: Any) {
        makeCall(phone: "89647777558")
    }
    
    @IBAction func managerCallButtonPressed(_ sender: Any) {
        makeCall(phone: "89251355526")
    }

    @IBAction func directorCallButtonPressed(_ sender: Any) {
        makeCall(phone: "84994094649")
    }
    
    // MARK: Methods
    func makeCall(phone: String) {
        if let url = URL(string: "telprompt://\(phone)") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
        }
        dismiss(animated: true, completion: nil)
    }
}
