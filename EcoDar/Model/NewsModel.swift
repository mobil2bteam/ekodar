//
//  NewsModel.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import ObjectMapper

class NewsModel: NSObject, Mappable {
    var id: Int?
    var text: String?
    var image: String?
    var image_big: String?
    var name: String?
    var add_text: String?
    var date: String?
    var date_from: String?
    var date_to: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        text    <- map["text"]
        image    <- map["image"]
        image_big    <- map["image_big"]
        name    <- map["name"]
        add_text    <- map["add_text"]
        date    <- map["date"]
        date_from    <- map["date_from"]
        date_to    <- map["date_to"]
    }
}
