//
//  SignInViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD

class SignInViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // MARK: - Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.addIcon(iconName: "User")
        passwordTextField.addIcon(iconName: "Password")
    }
    
    // MARK: - Methods
    @IBAction func signInButtonPressed(_ sender: Any) {
        view.endEditing(true)
        if (passwordTextField.text?.isEmpty)! ||  (emailTextField.text?.isEmpty)! {
            alert(message: "Заполните все поля")
        } else {
            let email = emailTextField.text!
            let password = passwordTextField.text!
            weak var weakSelf = self
            SVProgressHUD.show()
            APIManager.signIn(email: email, password: password, successHandler: {
                SVProgressHUD.dismiss()
                weakSelf?.showMainController()
            }) { (error) in
                SVProgressHUD.dismiss()
                weakSelf?.alert(message: error)
            }
        }
    }
    
    func showMainController() {
        let navVC = UINavigationController.init(rootViewController: MainViewController.init())
        present(navVC, animated: true, completion: nil)
    }

    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }

}
