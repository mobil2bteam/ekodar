//
//  ProductModel.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import ObjectMapper
import RealmSwift

class ProductRealmModel: Object{
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var count: Int = 0
    dynamic var image: String = ""
    dynamic var offer_id: Int = 0
    dynamic var price: Int = 0

    override static func primaryKey() -> String? {
        return "id"
    }
}

class ProductModel: NSObject, Mappable, NSCopying {
    var id: Int = 0
    var image: String = ""
    var images: [String] = []
    var flags: [FlagModel] = []
    var properties: [PropertyModel] = []
    var name: String!
    var short_description: String?
    var description_: String?
    var url: String?
    var prices: [PriceModel] = []
    var count: Int = 0
    
    required init?(map: Map) {
        
    }
    
    func getImageURL() -> URL? {
        if images.count > 0 {
            if let imageUrl = URL(string: images[0]) {
                return imageUrl
            }
        }
        return nil
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        image    <- map["image"]
        name    <- map["name"]
        short_description    <- map["short_description"]
        description_    <- map["description"]
        url    <- map["url"]
        prices <- map["prices"]
        images <- map["images"]
        properties <- map["properties"]
        flags <- map["flags"]
    }
    
    required init(_ model: ProductModel) {
        id = model.id
        image = model.image
        images = model.images
        flags = model.flags
        properties = model.properties
        name = model.name
        short_description = model.short_description
        description_ = model.description_
        url = model.url
        let originalArray = model.prices as NSArray
        let duplicateArray = NSArray(array:originalArray as! [PriceModel], copyItems: true)
        prices = duplicateArray as! [PriceModel]
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return type(of:self).init(self)
    }
}

class PriceModel: NSObject, NSCopying, Mappable {
    var offer_id: Int?
    var oldPrice: Int?
    var price: Int!
    var weight: Int?
    var count: Int?
    
    required init?(map: Map) {
        
    }
    
    init(total: Int, price: Int, count: Int) {
        self.price = price
    }

    func mapping(map: Map) {
        weight    <- map["weight"]
        price    <- map["price"]
        oldPrice    <- map["oldPrice"]
        offer_id    <- map["id"]
        count    <- map["count"]
    }
    
    required init(_ model: PriceModel) {
        offer_id = model.offer_id
        oldPrice = model.oldPrice
        price = model.price
        weight = model.weight
        count = model.count
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return type(of:self).init(self)
    }
    
}

class FlagModel: NSObject, NSCopying, Mappable {
    var id: Int!
    var name: String!
    var color: String!
    var text_color: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        name    <- map["name"]
        color    <- map["color"]
        text_color    <- map["text_color"]
    }
    
    required init(_ model: FlagModel) {
        id = model.id
        name = model.name
        color = model.color
        text_color = model.text_color
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return type(of:self).init(self)
    }
}

class PropertyModel: NSObject, NSCopying, Mappable {
    var id: Int?
    var key: String?
    var value: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        key    <- map["key"]
        value    <- map["value"]
    }
    
    required init(_ model: PropertyModel) {
        id = model.id
        key = model.key
        value = model.value
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return type(of:self).init(self)
    }
}


