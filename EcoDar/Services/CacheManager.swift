//
//  CacheManager.swift
//  EcoDar
//
//  Created by Ruslan on 10/5/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import Foundation
import RealmSwift

class CacheManager: NSObject {
    
    /**
     Remove cache which is expired.
     */
    class func updateCache() {
        for cache in uiRealm.objects(CacheRealmModel.self) {
            if cache.expiryDate().compare(Date()) == .orderedAscending {
                try! uiRealm.write{
                    uiRealm.delete(cache)
                }
            }
        }
    }
    
    /**
     Clear all cache.
     */
    class func clearAllCache() {
        try! uiRealm.write{
            uiRealm.delete(uiRealm.objects(CacheRealmModel.self))
        }
    }

    /**
     Add cache
     - parameter cache: Cache need to be added.
     */
    class func addCache(cache: CacheRealmModel) {
        try! uiRealm.write{
            uiRealm.add(cache)
        }
    }
    
    /**
     Find and get cache for url request.
     - parameter url: Url for request.
     - parameter parameters: Parameters for request, can be nil.
     - returns: cache if it was found, otherwise nil.
     */
    class func cache(for url: String, parameters: Dictionary<String, String>? = nil) -> CacheRealmModel? {
        let cacheResults = uiRealm.objects(CacheRealmModel.self)
        for result in cacheResults {
            if result.url == url {
                if parameters == nil  {
                    return result
                } else {
                    if  let parametersString = parameters!.convertToString(), result.parameters! == parametersString {
                        return result
                    }
                }
            }
        }
        return nil
    }
}
