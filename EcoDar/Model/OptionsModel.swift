//
//  OptionsModel.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import ObjectMapper

class OptionsModel: Mappable {
    var cache: Int?
    var email: String!
    var urls: [UrlModel] = []
    var catalog: [CatalogModel] = []
    var news: [NewsModel] = []
    var user: UserModel? {
        didSet {
            if user != nil {
                NotificationManager.generateNotifications(for: user!.info.route)
            }
        }
    }

    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        cache    <- map["cache"]
        urls     <- map["options.urls"]
        catalog  <- map["catalog.items.0.items"]
        news  <- map["news.items"]
        user  <- map["user"]
        email <- map["settings.company_info.contacts.email"]
    }
    
    func urlFor(name: String) -> UrlModel? {
        for url in urls {
            if url.name == name {
                return url
            }
        }
        return nil
    }
}

class UrlModel: Mappable {
    var name: String?
    var url: String?
    var metod: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        name    <- map["name"]
        url    <- map["url"]
        metod    <- map["metod"]
    }
}
