//
//  UITableViewCellExtension.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import Alamofire

extension UITableViewCell {
    
    class func nib() -> UINib {
        let name = String(describing: self)
        return UINib.init(nibName: name, bundle: nil)
    }

}

extension UICollectionViewCell {
    
    class func nib() -> UINib {
        let name = String(describing: self)
        return UINib.init(nibName: name, bundle: nil)
    }
    
}

extension UIViewController {
    
    convenience init(nibName: String = String(describing: self)){
        self.init(nibName: nibName, bundle: nil)
    }

    class func nib() -> UINib {
        let name = String(describing: self)
        return UINib.init(nibName: name, bundle: nil)
    }
    
}

extension String {
    
    var html2AttributedString: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
