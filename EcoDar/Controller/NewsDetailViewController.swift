//
//  NewsDetailViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import AlamofireImage

class NewsDetailViewController: UIViewController {
    var news: NewsModel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Lifecycle
    convenience init(news: NewsModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.news = news
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Статья"
        newsLabel.text = news.name
        let imageUrl = URL(string: news.image_big ?? "")!
        newsImageView.af_setImage(withURL: imageUrl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        descriptionLabel.text = try! news.text?.convertHtmlSymbols()
    }
}
