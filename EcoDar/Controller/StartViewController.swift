//
//  StartViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SVProgressHUD
import SideMenu

class StartViewController: UIViewController {
    
    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadOptions()
    }

    // MARK: Methods
    func loadOptions() {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.getOptions(successHandler: {
            SVProgressHUD.dismiss()
            weakSelf?.showMainController()
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error.localizedDescription, title: "", handler: { (action) in
                weakSelf?.loadOptions()
            })
        }
    }
    
    func showMainController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if AppManager.shared.options.user != nil {
            let navViewController = UINavigationController.init(rootViewController: MainViewController.init())
            appDelegate.window?.rootViewController = navViewController
        } else {
            appDelegate.window?.rootViewController = SignInViewController.init()
        }
    }
}
