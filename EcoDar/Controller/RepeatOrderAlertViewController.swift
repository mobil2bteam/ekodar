//
//  RepeatOrderAlertViewController.swift
//  EcoDar
//
//  Created by Ruslan on 10/6/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class RepeatOrderAlertViewController: UIViewController {
    var completionHandler: ((_ addCartProducts:Bool) -> Void) = { _ in }
    
    // MARK: - Lifecyle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    // MARK: - Actions
    @IBAction func yesButtonPressed(_ sender: Any) {
        weak var weakSelf = self
        dismiss(animated: true) { 
            weakSelf?.completionHandler(true)
        }
    }
 
    @IBAction func noButtonPressed(_ sender: Any) {
        weak var weakSelf = self
        dismiss(animated: false) {
            weakSelf?.completionHandler(false)
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
