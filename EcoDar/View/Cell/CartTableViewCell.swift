//
//  CartTableViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import AlamofireImage

class CartTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var countTextField: UITextField!
    var product: ProductRealmModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        plusButton.setBackgroundImage(UIImage.init(color: UIColor.plusButtonBackgroundColor()), for: .normal)
        plusButton.setBackgroundImage(UIImage.init(color: UIColor.plusButtonBackgroundColorHighlighted()), for: .highlighted)
        minusButton.setBackgroundImage(UIImage.init(color: UIColor.minusButtonBackgroundColor()), for: .normal)
        minusButton.setBackgroundImage(UIImage.init(color: UIColor.minusButtonBackgroundColorHighlighted()), for: .highlighted)
        countTextField.delegate = self
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        product = nil
    }

    // MARK: Configuration
    func configure(product: ProductRealmModel) {
        self.product = product
        nameLabel.text = product.name
        countTextField.text = "\(product.count)"
        priceLabel.text = "\(product.price) руб."
        if let imageUrl = URL(string: product.image) {
            productImageView.af_setImage(withURL: imageUrl)
        }
    }
    
    func configure(orderProduct: ProductModel) {
        countTextField.isHidden = true
        minusButton.isHidden = true
        plusButton.isHidden = true
        deleteButton.isHidden = true
        nameLabel.text = orderProduct.name
        if orderProduct.prices.count > 0 {
            countTextField.text = "\(orderProduct.prices[0].count ?? 0)"
            priceLabel.text = "\(orderProduct.prices[0].price!) руб."
        }
        if let imageUrl = URL(string: orderProduct.image) {
            productImageView.af_setImage(withURL: imageUrl)
        }
    }

    // MARK: Actions
    @IBAction func minusButtonPressed(_ sender: Any) {
        if product != nil && product!.count > 1 {
            DataManager.updateProduct(productId: product!.id, count: product!.count - 1)
        }
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        if product != nil {
            DataManager.updateProduct(productId: product!.id, count: product!.count + 1)
        }
    }

    @IBAction func deleteButtonPressed(_ sender: Any) {
        if product != nil {
            DataManager.deleteProduct(product: product!)
        }
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            var newCount = 0
            if text.characters.count > 0 {
                newCount = Int(text)!
            }
            DataManager.updateProduct(productId: product!.id, count: newCount)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if let text = textField.text {
            return text.characters.count < 3
        }
        return true
    }
}
