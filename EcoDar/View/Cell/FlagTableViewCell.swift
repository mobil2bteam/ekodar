//
//  FlagTableViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 10/4/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class FlagTableViewCell: UITableViewCell {

    @IBOutlet weak var flagView: UIView!
    @IBOutlet weak var flagLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(for flag:FlagModel) {
        flagLabel.text = flag.name
        flagLabel.textColor = UIColor(flag.text_color)
        flagView.backgroundColor = UIColor(flag.color)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        flagLabel.text = " "
    }
}
