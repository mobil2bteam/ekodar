//
//  OrderViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    var completionHandler: () -> Void = {}

    // MARK: - Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        AppManager.shared.options.user!.removeNotificationForToday()
        okButton.backgroundColor = UIColor.accentColor()
    }
    
    // MARK: - Methods
    @IBAction func okButtonPressed(_ sender: Any) {
        self.completionHandler()
        dismiss(animated: true, completion: nil)
    }

}
