//
//  ProductCollectionViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductCollectionViewCell: UICollectionViewCell, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var flagsTableView: UITableView!
    @IBOutlet weak var countTextField: UITextField!
    var flags: [FlagModel] = []
    var canBeAdded = false
    var count: Int = 0
    var product: ProductModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        plusButton.setBackgroundImage(UIImage.init(color: UIColor.plusButtonBackgroundColor()), for: .normal)
        plusButton.setBackgroundImage(UIImage.init(color: UIColor.plusButtonBackgroundColorHighlighted()), for: .highlighted)
        minusButton.setBackgroundImage(UIImage.init(color: UIColor.minusButtonBackgroundColor()), for: .normal)
        minusButton.setBackgroundImage(UIImage.init(color: UIColor.minusButtonBackgroundColorHighlighted()), for: .highlighted)
        countTextField.delegate = self
        flagsTableView.dataSource = self
        flagsTableView.register(FlagTableViewCell.nib(), forCellReuseIdentifier: "cell")
    }

    func configure(for product: ProductModel) {
        self.product = product
        if product.prices.count > 0 {
            canBeAdded = true
            priceLabel.text = "\(product.prices[0].price!) руб."
            countTextField.isUserInteractionEnabled = true
            if product.prices[0].oldPrice != nil && product.prices[0].oldPrice! > 0 {
                let text = "\(product.prices[0].oldPrice!) руб."
                let attrString = NSAttributedString(string: text, attributes: [NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
                oldPriceLabel.attributedText = attrString
            }
        }
        flags = product.flags
        flagsTableView.reloadData()
        nameLabel.text = product.name
        count = DataManager.countFor(product: product)
        countTextField.text = "\(count)"
        if let imageURL = product.getImageURL() {
            productImageView.af_setImage(withURL: imageURL)
        }
    }
    
    override func prepareForReuse() {
        canBeAdded = false
        countTextField.isUserInteractionEnabled = false
        countTextField.text = "0"
        flagsTableView.reloadData()
        nameLabel.text = ""
        priceLabel.text = " "
        oldPriceLabel.text = " "
        count = 0
        productImageView.image = nil
    }
    
    @IBAction func minusButtonPressed(_ sender: Any) {
        if canBeAdded {
            DataManager.decreaseProduct(productId: product!.id)
            count = DataManager.countFor(product: product!)
            countTextField.text = "\(count)"
        }
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        if canBeAdded {
            DataManager.addProduct(product: product!)
            count = DataManager.countFor(product: product!)
            countTextField.text = "\(count)"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            var newCount = 0
            if text.characters.count > 0 {
                newCount = Int(text)!
            }
            DataManager.addProduct(product: product!, count: newCount)
            count = DataManager.countFor(product: product!)
            countTextField.text = "\(count)"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }
        if let text = textField.text {
            return text.characters.count < 3
        }
        return true
    }

}

extension ProductCollectionViewCell {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FlagTableViewCell
        cell.configure(for: flags[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
}

