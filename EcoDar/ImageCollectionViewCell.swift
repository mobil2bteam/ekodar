//
//  ImageCollectionViewCell.swift
//  EcoDar
//
//  Created by Ruslan on 10/4/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import AlamofireImage

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(for image: String) {
        if let imageUrl = URL(string: image) {
            imageView.af_setImage(withURL: imageUrl)
        }
    }
    
    override func prepareForReuse() {
        imageView.image = nil
    }
}
