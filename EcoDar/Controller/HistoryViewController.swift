//
//  HistoryViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD

class HistoryViewController: RootViewController, UITableViewDataSource, UITableViewDelegate{
    let cellIdentifier = "OrderTableViewCell"
    @IBOutlet weak var orderCountLabel: UILabel!
    @IBOutlet weak var historyTableView: UITableView!
    var orders: [OrderModel] = []

    // MARK: - Lifecyle
    convenience init(orders: [OrderModel], showHamburgerButton: Bool) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.showHamburgerButton = showHamburgerButton
        self.orders = orders
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "История заказов"
        historyTableView.register(OrderTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        orderCountLabel.text = "Заказов: \(orders.count)"
    }
    
    // MARK: - Methods
    func showOrderDetailViewController(for order: OrderModel) {
        let orderDetailViewController = OrderDetailViewController.init(order: order.copy() as! OrderModel)
        navigationController?.pushViewController(orderDetailViewController, animated: true)
    }
}

extension HistoryViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OrderTableViewCell
        cell.configure(for: orders[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order = orders[indexPath.row]
        showOrderDetailViewController(for: order)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
