//
//  CartViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import RealmSwift
import SVProgressHUD

class CartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var orderButton: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var emptyCartView: UIView!
    let cellIdentifier = "OrderTableViewCell"

    // MARK: - Lifecyle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Корзина"
        totalPriceLabel.text = "\(DataManager.totalPrice()) руб."
        orderButton.backgroundColor = UIColor.accentColor()
        cartTableView.register(CartTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.updateProductCount), name: DataManager.notificationName, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emptyCartView.isHidden = DataManager.products().count > 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: DataManager.notificationName, object: nil)
    }
    
    // MARK: Methods
    func updateProductCount() {
        cartTableView.reloadData()
        totalPriceLabel.text = "\(DataManager.totalPrice()) руб."
        emptyCartView.isHidden = DataManager.products().count > 0
    }

    // MARK: Actions
    @IBAction func orderButtonPressed(_ sender: Any) {
        var idArray = [String]()
        for product in DataManager.products() {
            idArray.append("\(product.offer_id)-\(product.count)")
        }
        SVProgressHUD.show()
        weak var weakSelf = self
        APIManager.order(cart: idArray.joined(separator: ";"), successHandler: { (order) in
            SVProgressHUD.dismiss()
            let orderViewController = OrderViewController.init()
            orderViewController.completionHandler = {
                weakSelf?.navigationController?.pushViewController(MainViewController.init(), animated: false)
            }
            weakSelf?.present(orderViewController, animated: true, completion: nil)
            DataManager.clear()
        }) { (error) in
            SVProgressHUD.dismiss()
            SVProgressHUD.showError(withStatus: error)
        }
    }
}


extension CartViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.products().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CartTableViewCell
        cell.configure(product: DataManager.products()[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
