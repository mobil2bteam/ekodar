//
//  MenuViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import MessageUI
import SideMenu
import SVProgressHUD

class MenuViewController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet var menuButtonsArray: [UIButton]!
    
    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        for button in menuButtonsArray {
            let titleColor = UIColor.menuButtonTitleColor()
            button.setTitleColor(titleColor, for: .normal)
        }
    }

    // MARK: Methods
    func loadOrders() {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.orders(successHandler: { (orderList) in
            let vc = HistoryViewController.init(orders: orderList.actual + orderList.archive, showHamburgerButton: true)
            weakSelf?.navigationController?.pushViewController(vc, animated: true)
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    // MARK: Actions
    @IBAction func homeButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(MainViewController.init(), animated: false)
    }

    @IBAction func companyButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(AboutViewController.init(), animated: false)
    }

    @IBAction func historyButtonPressed(_ sender: Any) {
        loadOrders()
    }
    
    @IBAction func newsButtonPressed(_ sender: Any) {
        let newsViewController = NewsViewController.init(showHamburgerButton: true)
        navigationController?.pushViewController(newsViewController, animated: false)
    }
    
    @IBAction func callButtonPressed(_ sender: Any) {
        let phoneAlert = PhoneAlertViewController.init()
        phoneAlert.modalPresentationStyle = .overFullScreen
        phoneAlert.modalTransitionStyle = .crossDissolve
        present(phoneAlert, animated: true, completion: nil)
    }
    
    @IBAction func writeButtonPressed(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            alert(message: "Ваше устройство не поддерживает отправку писем")
            return
        }
        sendEmail()
    }

}

// MARK: MFMailComposeViewControllerDelegate
extension MenuViewController {
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["info@ekodar.ru"])
        composeVC.setSubject("Сообщение с мобильного приложения")
        composeVC.setMessageBody("", isHTML: false)
        present(composeVC, animated: true, completion: nil)
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
