//
//  RootViewController.swift
//  EcoDar
//
//  Created by Ruslan on 10/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SideMenu

class RootViewController: UIViewController {
    var showHamburgerButton: Bool = false
    var showCartButton: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        if showHamburgerButton == true {
            let leftBarButton = UIBarButtonItem.init(image: UIImage.init(named: "List"), style: .done, target: self, action: #selector(self.leftButtonAction))
            navigationItem.leftBarButtonItem = leftBarButton
        }
        if showCartButton == true {
            let rightBarButton = UIBarButtonItem.init(image: UIImage.init(named: "Van"), style: .done, target: self, action: #selector(self.rightButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        }     
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if showCartButton {
            let number = DataManager.totalCount()
            navigationItem.rightBarButtonItem?.setBadge(text: "\(number)")
        }
    }
    
    // MARK: - Actions
    func leftButtonAction(sender: UIBarButtonItem) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func rightButtonAction(sender: UIBarButtonItem) {
        let cartViewController = CartViewController.init(nibName: "CartViewController", bundle: nil)
        navigationController?.pushViewController(cartViewController, animated: true)
    }

}
