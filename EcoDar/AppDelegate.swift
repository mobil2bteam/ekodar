//
//  AppDelegate.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SideMenu
import SVProgressHUD
import IQKeyboardManagerSwift
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        application.applicationIconBadgeNumber = 0
        Fabric.with([Crashlytics.self])
        NotificationManager.registerLocalNotification()
        NotificationManager.removeAllNotifications()
        CacheManager.updateCache()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Готово"

        prepareAppearance()
        prepareProgressHUD()
        prepareSideMenuManager()
        prepareWindow()
        return true
    }

    func prepareProgressHUD() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setMinimumDismissTimeInterval(3)
    }
    
    func prepareSideMenuManager() {
        SideMenuManager.menuPushStyle = .replace
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 0
        SideMenuManager.menuWidth = 200
    }
    
    func prepareAppearance() {
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.primaryColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func prepareWindow() {
        let menuViewController = MenuViewController.init()
        let startViewController = StartViewController.init()
        let leftNavigationController = UISideMenuNavigationController(rootViewController: menuViewController)
        leftNavigationController.leftSide = true
        SideMenuManager.menuLeftNavigationController = leftNavigationController
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white
        window?.rootViewController = startViewController
        window?.makeKeyAndVisible()
    }
}

