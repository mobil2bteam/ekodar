//
//  UserModel.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import ObjectMapper

typealias UserNotification = (shouldShow: Bool, message: String?)

var showNotificationView = true

class UserModel: Mappable {
    var info: InfoModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        info    <- map["info"]
    }
    
    func removeNotificationForToday() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d.MM.y"
        let today = dateFormatter.string(from: Date())
        UserDefaults.standard.set(true, forKey: today)
        NotificationManager.removeAllNotifications()
        NotificationManager.generateNotifications(for: AppManager.shared.options.user!.info.route)
    }
    
    func shouldShowNotificationForToday() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d.MM.y"
        let today = dateFormatter.string(from: Date())
        return !UserDefaults.standard.bool(forKey: today) && showNotificationView
    }
    
    func hasNotificationForToday() -> UserNotification {
        if info.route.schedule.count == 0 || !shouldShowNotificationForToday() {
            return (false, nil)
        }
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "ru_RU")
        dateFormatter.dateFormat  = "EEEE"
        let dayInWeek = dateFormatter.string(from: date as Date)
        
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.hour, .minute], from: Date())
        let hour = comp.hour
        let minute = comp.minute
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat  = "HH:mm"
        
        for day in info.route.schedule {
            if day.week_name.caseInsensitiveCompare(dayInWeek) == .orderedSame {
                // compare time
                let startTime = timeFormatter.date(from: day.time_from)
                let endTime = timeFormatter.date(from: day.time_to)
                let currentTime = timeFormatter.date(from: "\(hour!):\(minute!)")
                if startTime != nil, endTime != nil, currentTime != nil {
                    if  currentTime?.compare(endTime!) == .orderedAscending {
                        let notification = "Не забудьте сегодня сделать заказ до \(day.time_to!)"
                        return (true, notification)
                    }
                }
            }
        }
        return (false, nil)
    }

}

class InfoModel: Mappable {
    var id: Int!
    var accessToken: String!
    var email: String!
    var phone: String!
    var name: String!
    var login: String!
    var route: RouteModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        accessToken    <- map["accessToken"]
        email    <- map["email"]
        phone    <- map["phone"]
        login    <- map["login"]
        name    <- map["name"]
        route   <- map["route"]
    }
}

class RouteModel: Mappable {
    var id: Int!
    var name: String!
    var schedule: [ScheduleModel]!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        schedule    <- map["schedule"]
        name    <- map["name"]
    }
    
    func hasOrderedToday() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "d.MM.y"
        let today = dateFormatter.string(from: Date())
        return UserDefaults.standard.bool(forKey: today)
    }
    
}

class ScheduleModel: Mappable {
    var week: Int!
    var week_name: String!
    var time_from: String!
    var time_to: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        week    <- map["week"]
        week_name    <- map["week_name"]
        time_from    <- map["time_from"]
        time_to    <- map["time_to"]
    }
}
