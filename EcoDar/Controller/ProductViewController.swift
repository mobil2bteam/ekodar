//
//  ProductViewController.swift
//  EcoDar
//
//  Created by Ruslan on 9/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class ProductViewController: RootViewController , UITableViewDataSource {
    var product: ProductModel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var flagsTableView: UITableView!
    let cellIdentifier = "FlagTableViewCell"

    // MARK: Lifecycle
    convenience init(product: ProductModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.product = product
        self.showCartButton = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Товар"
        priceView.backgroundColor = UIColor.accentColor()
        flagsTableView.register(FlagTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        prepareViews()
        NotificationCenter.default.addObserver(self, selector: #selector(ProductViewController.updateProductCount), name: DataManager.notificationName, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: DataManager.notificationName, object: nil)
    }

    // MARK: Methods
    func updateProductCount() {
        if showCartButton {
            let number = DataManager.totalCount()
            navigationItem.rightBarButtonItem?.setBadge(text: "\(number)")
        }
    }

    func prepareViews() {
        if product.prices.count > 0 {
            priceLabel.text = "\(product.prices[0].price!) руб."
        }
        if let description = product.description_ {
            if description.characters.count > 0 {
                descriptionLabel.text = try! description.convertHtmlSymbols()
                descriptionView.isHidden = false
            }
        }
        productLabel.text = product.name
        countLabel.text = "\(DataManager.countFor(product: product))"
        if let imageURL = product.getImageURL() {
            productImageView.af_setImage(withURL: imageURL)
        }
    }
    
    // MARK: Actions
    @IBAction func minusButtonPressed(_ sender: Any) {
        if product.prices.count > 0 {
            DataManager.decreaseProduct(productId: product.id)
            countLabel.text = "\(DataManager.countFor(product: product))"
        }
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        if  product.prices.count > 0 {
            DataManager.addProduct(product: product)
            countLabel.text = "\(DataManager.countFor(product: product))"
        }
    }
}

extension ProductViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product.flags.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FlagTableViewCell
        cell.configure(for: product.flags[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }    
}

