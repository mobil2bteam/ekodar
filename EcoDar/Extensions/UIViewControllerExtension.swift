//
//  UIViewControllerExtension.swift
//  EcoDar
//
//  Created by Ruslan on 9/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func alert(message: String, title: String = "", handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }

}
