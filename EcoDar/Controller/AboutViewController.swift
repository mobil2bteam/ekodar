//
//  AboutViewController.swift
//  EcoDar
//
//  Created by Ruslan on 10/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import SideMenu

class AboutViewController: RootViewController {

    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.showHamburgerButton = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "О приложении"
    }
}
