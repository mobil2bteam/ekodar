//
//  APIManager.swift
//  EcoDar
//
//  Created by Ruslan on 9/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON


class APIManager: NSObject {

    static let appKey = "000000"
    static let baseURL = "http://ekodar.mobil2b.com/api"
    
    class func getOptions (successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 120

        let urls = ["url_catalog", "url_news", "url_settings"]
        var parameters = ["key": appKey, "urls": urls.joined(separator: ",")]

        // if user was logged in
        if let token = UserDefaults.standard.string(forKey: "token") {
            parameters["token"] = token
        }
 
        if let existCache = CacheManager.cache(for: baseURL, parameters: parameters) {
            let options = Mapper<OptionsModel>().map(JSON: existCache.response.convertToDictionary()!)
            AppManager.shared.options = options
            successHandler()
            return
        }

        Alamofire.SessionManager.default.request(URL(string: baseURL)!, method: .get, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!)
                    return
                }
                // map options
                let responseJSON = response.result.value as? [String: Any]
                let options = Mapper<OptionsModel>().map(JSON: responseJSON!)
                AppManager.shared.options = options
                
                // Cache options
                if let cacheTime = options?.cache {
                    let cacheResponse = CacheRealmModel(url: baseURL, response: responseJSON!, seconds: cacheTime, parameters: parameters)
                    CacheManager.addCache(cache: cacheResponse)
                }
                
                successHandler()
        }
    }
    
    class func getProductsFor (catalogId: Int, successHandler: @escaping ([ProductModel]) -> Void, errorHandler: @escaping (Error) -> Void) {
        
        let parameters = ["key": appKey, "catalog_id": "\(catalogId)"]
        let url = AppManager.shared.options.urlFor(name: "url_products")
        let method = HTTPMethod(rawValue: (url?.metod?.uppercased())!)
        
        if let existCache = CacheManager.cache(for: (url?.url)!, parameters: parameters) {
            if let productDictionary = existCache.response.convertToDictionary(),
                let productsArray = productDictionary["products"] as? [String: Any],
                let items = productsArray["items"] as? [[String: Any]] {
                let products = Mapper<ProductModel>().mapArray(JSONArray: items)
                successHandler(products)
                return
            }
        }

        Alamofire.SessionManager.default.request((url?.url)!, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                var products: [ProductModel] = []
                switch response.result {
                case .success:
                    if let data = response.data {
                        let json = JSON(data: data)
                        // map catalog
                        if let productsList = json["products"]["items"].arrayObject as? [[String: Any]] {
                            products = Mapper<ProductModel>().mapArray(JSONArray: productsList)
                            // Cache products
                            if let responseJSON = response.result.value as? [String: Any],
                                let cacheTime = responseJSON["cache"] as? Int {
                                    let cacheResponse = CacheRealmModel(url: (url?.url)!, response: responseJSON, seconds: cacheTime, parameters: parameters)
                                    CacheManager.addCache(cache: cacheResponse)
                            }
                        }
                    }
                    successHandler(products)
                case .failure(let error):
                    errorHandler(error)
                }
        }
    }

    class func getProductFor (productId: Int, successHandler: @escaping (ProductModel) -> Void, errorHandler: @escaping (String) -> Void) {

        let parameters = ["key": appKey, "product_id": "\(productId)"]
        let url = AppManager.shared.options.urlFor(name: "url_product")
        let method = HTTPMethod(rawValue: (url?.metod?.uppercased())!)
        
        if let existCache = CacheManager.cache(for: (url?.url)!, parameters: parameters) {
            if let productDictionary = existCache.response.convertToDictionary(){
                let product = Mapper<ProductModel>().map(JSON: productDictionary)
                successHandler(product!)
                return
            }
        }
        Alamofire.SessionManager.default.request((url?.url)!, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                if let responseJSON = response.result.value as? [String: Any],
                    let productJSON = responseJSON["product"] as? [String: Any] {
                    let product = Mapper<ProductModel>().map(JSON: productJSON)
                    
                    // Cache product
                    if let cacheTime = responseJSON["cache"] as? Int {
                        let cacheResponse = CacheRealmModel(url: (url?.url)!, response: productJSON, seconds: cacheTime, parameters: parameters)
                        CacheManager.addCache(cache: cacheResponse)
                    }
                    successHandler(product!)
                }
        }
    }

    // MARK: User
    class func signIn(email: String, password: String, successHandler: @escaping () -> Void, errorHandler: @escaping (String) -> Void) {
        let parameters = ["key": appKey, "login": email, "password": password]
        let url = AppManager.shared.options.urlFor(name: "url_login")
        let method = HTTPMethod(rawValue: (url?.metod?.uppercased())!)
        Alamofire.SessionManager.default.request((url?.url)!, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard let userDictionary = responseJSON!["user"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                // map user
                let user = Mapper<UserModel>().map(JSON: userDictionary)
                AppManager.shared.options.user = user
                // save token
                UserDefaults.standard.set(user!.info.accessToken, forKey: "token")
                UserDefaults.standard.synchronize()
                successHandler()
        }
    }
    
    class func order(cart: String, successHandler: @escaping (OrderModel) -> Void, errorHandler: @escaping (String) -> Void) {
        let token = AppManager.shared.options.user!.info.accessToken ?? ""
        let parameters = ["key": appKey, "token": token, "cart": cart]
        let url = AppManager.shared.options.urlFor(name: "url_order")
        let method = HTTPMethod(rawValue: (url?.metod?.uppercased())!)
        Alamofire.SessionManager.default.request((url?.url)!, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                
                let responseJSON = response.result.value as? [String: Any]
                guard (responseJSON!["order"] as? [String: Any]) != nil else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                if let orderDictionary = responseJSON?["order"] as? [String: Any] {
                    let order = Mapper<OrderModel>().map(JSON: orderDictionary)
                    successHandler(order!)
                }
        }
    }

    class func orders(successHandler: @escaping (OrderListModel) -> Void, errorHandler: @escaping (String) -> Void) {
        let token = AppManager.shared.options.user!.info.accessToken ?? ""
        let parameters = ["key": appKey, "token": token]
        let url = AppManager.shared.options.urlFor(name: "url_user_orders")
        let method = HTTPMethod(rawValue: (url?.metod?.uppercased())!)
        Alamofire.SessionManager.default.request((url?.url)!, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard (responseJSON!["orders"] as? [String: Any]) != nil else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                if let orderDictionary = responseJSON?["orders"] as? [String: Any] {
                    let orderList = Mapper<OrderListModel>().map(JSON: orderDictionary)
                    successHandler(orderList!)
                }
        }
    }

}
